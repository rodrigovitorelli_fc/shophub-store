'use strict'
const inquirer = require('inquirer')
let inquirerJSON
const { exec } = require('child_process')

try {
  inquirerJSON = require('./inquirer.json')
}
catch (error) {
  inquirerJSON = false
}


if (!inquirerJSON) {
  console.log('\x1b[36m%s\x1b[0m', ' \n Configuração de inicialização do BuilderIO: \n')

  const questions = [
    {
      type: 'input',
      name: 'name',
      message: 'Qual o nome da loja?',
      default: 'builderio',
    },
    {
      type: 'input',
      name: 'accountName',
      message: 'Qual o accountName da loja?',
      default: 'agenciabluefootio',
      validate: function (value) {
        return (!(value.match(/[\ *áéíóúâêîôûãẽĩõũç\\;?.,&@+]/ig)))
          ? true
          : 'Por favor, use um accountName válido sem espaços, acentuação ou símbolos \n'
      },
      filter: function (val) {
        return val.toLowerCase()
      },
    }   
  ]

  inquirer.prompt(questions)
    .then(function (answers) {
      console.log('\nPreparando o arquivo inquirer.json...')
      const str = JSON.stringify(answers, null, '  ')
      const fs = require('fs')
      let man = require('./manifest.json')

      man['name'] = answers.name
      man['vendor'] = answers.accountName

      let pkg = require('./package.json')
      pkg['name'] = answers.accountName

      man = JSON.stringify(man, null, '  ')
      fs.writeFile('manifest.json', man, () => console.log('manifest.json atualizado! \n'))

      pkg = JSON.stringify(pkg, null, '  ')
      fs.writeFile('package.json', pkg, () => console.log('package.json atualizado! \n'))


      fs.writeFile('inquirer.json', str, () => console.log('Projeto configurado! \n'))
    })
} else {
  console.log('\x1b[36m%s\x1b[0m', ' \n Projeto já configurado... \n')
}
