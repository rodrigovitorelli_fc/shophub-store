'use strict';

const autoprefixer = require('gulp-autoprefixer');
const csso = require('gulp-csso');
const del = require('del');
const gulp = require('gulp');
const sass = require('gulp-sass');
const watch = require('gulp-watch');
const plumber = require('gulp-plumber');
const notify = require('gulp-notify');
const path = require('path');

//the title and icon that will be used for the Grunt notifications
var notifyInfo = {
  title: 'Gulp',
  icon: path.join(__dirname, 'gulp.png')
};

//error notification settings for plumber
const plumberErrorHandler = {
  errorHandler: notify.onError({
    title: notifyInfo.title,
    icon: notifyInfo.icon,
    message: "Error: <%= error.message %>"
  })
};

sass.compiler = require("node-sass");

// Clean output directory
gulp.task('clean', () => del(['./styles/css']));

gulp.task('sass', function() {
  return gulp.src('./styles/scss/**/*.scss')
    .pipe(plumber(plumberErrorHandler))
    .pipe(sass())
    .pipe(autoprefixer())
    .pipe(csso())
    .pipe(gulp.dest('./styles/css'));
});

gulp.task('watch', () => {
  gulp.watch('./styles/scss/**/*.scss', gulp.series('sass'));
});

// Gulp task to minify all files
gulp.task('default', gulp.series('clean', gulp.parallel('watch', 'sass')));
